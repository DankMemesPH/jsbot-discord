# README #

You need to install **discord.js to run this bot**.

# Where can I install discord.js? #

1. Go to https://github.com/hydrabolt/discord.js/ ( Exit this after installing Discord.js)
2. Open up your **Node.js Command Prompt**
3. Type in **"npm install hydrabolt/discord.js#indev"**
4. Wait for it to install.

# Where can I install Node.js? #

1. Go to https://nodejs.org/en/
2. Click on the **LTS Version**
3. Install it.

# How to run this? #

1. Download the **master** branch
2. Extract it to any directory you want.
3. **Shift+RMB** in the File Explorer and click **Open Command Window Here**
4. Type in **"node bot.js"**
5. Wait for it to say **"bot.js Launched"**