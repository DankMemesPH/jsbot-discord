// JSBot made by DankMemesPH
// Date Created: 7/31/2016
// Date Updated: 8/7/2016
// Written in JavaScript

var Discord = require("discord.js")

var bot = new Discord.Client();

bot.on('ready', function() {
 console.log("bot.js Launched");
});

/* All of the Messages */

bot.on("message", function() {
 if(message.content === "Hello, JSBot") {
  bot.sendMessage(message, "Hi, " + message.author.username "Welcome to **" + message.channel.server.name + "**");
 }
});

bot.on("message", function() {
  if(message.content === "DO YOU LIKE COOKIES?") {
    bot.sendMessage(message, "I'm a bot, I can't answer that.");
  }
});

/* All of the Commands */

bot.on("message", function() {
 if(message.content === "$ServerName") {
  bot.sendMessage(message, "Server Name: **" + message.channel.server.name + "**");
 }
});

bot.on("message", function() {
  if(message.content === "$ChannelName") {
    bot.sendMessage(message, "Channel Name: **" + message.channel.name + "**");
  }
});

bot.on("message", function() {
 if(message.content === "$help") {
   bot.sendMessage(message, "**$ChannelName** *-* Shows the Name of the Channel");
   bot.sendMessage(message, "**$ServerName** *-* Shows the Name of the Server");
  }
});

bot.loginWithToken("Token Here");
